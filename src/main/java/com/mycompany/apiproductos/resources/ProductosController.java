/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apiproductos.resources;

import com.mycompany.apiproductos.dao.BazarProductosDao;
import com.mycompany.apiproductos.entity.Productos;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author arnaldoaraya
 */
@Path("/productos")
public class ProductosController {
    
    @GET
    @Path("/obtenerProductos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProductos(){
        BazarProductosDao bpzd = new BazarProductosDao();
        List<Productos> productos = bpzd.recuperarProducto();
        if(!productos.isEmpty()){
            return Response.ok().entity(productos).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Datos no Encotrados").build();
        }
        
    }
    
    @POST
    @Path("/agregarProducto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response agregarProductos(Productos producto){
        try {
            BazarProductosDao bpzd = new BazarProductosDao();
            bpzd.agregarProductos(producto);
            return Response
                    .status(Response.Status.CREATED)
                    .entity("Producto agregado correctamente.")
                    .build();
        } catch (Exception ex) {
            Logger.getLogger(ProductosController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }
    
    
    @DELETE
    @Path("/eliminarProducto/{codigo_producto}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarProducto(@PathParam("codigo_producto") int codigoProducto ){
        Productos productos = new Productos();
        productos.setCodigoProducto(codigoProducto);
        try{
            BazarProductosDao bpd = new BazarProductosDao();
            bpd.eliminarProducto(productos);
            return Response.ok().entity("Codigo eliminado correctamente.").build();
        }catch(Exception ex){
            Logger.getLogger(ProductosController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }
    
    
    @PUT
    @Path("/editarProducto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarProducto(Productos productos){
        BazarProductosDao bpd = new BazarProductosDao();
        try{
            bpd.editarProducto(productos);
            return Response.ok().entity("Se edito correctamente el producto").build();
        }catch(Exception ex){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }
    
}
