/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apiproductos.dao;

import com.mycompany.apiproductos.entity.Productos;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author arnaldoaraya
 */
public class BazarProductosDao {

    public void agregarProductos(Productos p)throws Exception{
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("productos");
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(p);
        entityManager.getTransaction().commit();
    }

    public List<Productos> recuperarProducto() {
        List<Productos> productos = new ArrayList<>();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("productos");
            EntityManager entityManager = emf.createEntityManager();

            TypedQuery<Productos> consultaAlumnos = entityManager.createNamedQuery("Productos.findAll", Productos.class);
            productos = consultaAlumnos.getResultList();
        } catch (Exception e) {
            e.getMessage();
        }

        return productos;
    }

    public void editarProducto(Productos producto)throws Exception{
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("productos");
        EntityManager entityManager = emf.createEntityManager();

        entityManager.getTransaction().begin();
        int key = producto.getCodigoProducto();
        Productos p = entityManager.find(Productos.class, key);
        //edito
        p.setDescripcionProducto(producto.getDescripcionProducto());
        p.setStockProducto(producto.getStockProducto());
        p.setValorProducto(producto.getValorProducto());

        entityManager.getTransaction().commit();
    }

    public void eliminarProducto(Productos producto)throws Exception{

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("productos");
        EntityManager entityManager = emf.createEntityManager();

        entityManager.getTransaction().begin();
        int key = producto.getCodigoProducto();
        Productos p = entityManager.find(Productos.class, key);
        entityManager.remove(p);

        entityManager.getTransaction().commit();

    }
}
